/**
Projekt:    Kostra 9. cviceni IZP 2015
Autor:      Marek Zak <izakmarek@fit.vutbr.cz>
Datum:      28. 11. 2015
*/

#include "array.h"

/**
 * Konstruktor pole. Vytvo�� pole o velikosti size a ka�d� prvek
 * v n�m inicializuje na hodnoty 0, NULL.
 */
Array array_ctor(unsigned size)
{
    Object *item = malloc(sizeof(Object)*size);
    //Nedostatok casu na kontrolu
    //if(item != NULL){

        for(int i = 0; i < size; i++){
            item[i] = object_ctor(0,NULL);
        }
        Array a = {size = size, item = item};
        return a;
    /*
    }
    else{
        return a;
    }
    */
}

/**
 * Zm�na velikosti pole. Zm�n�/realokuje zadan� pole na novou velikost.
 * V p��pad� zv�t�en� je�t� inicializuje ka�d� prvek na hodnoty 0, NULL
 */
Array array_resize(Array *arr, unsigned size)
{
    /** TODO: */
}

/**
 * Hled�n� prvku v poli podle identifik�toru prvku. Vrac� index prvku v poli
 * nebo -1, pokud prvek pole neobsahuje.
 */
int array_find_id(Array *arr, int id)
{
    for(int i = 0; i < arr->size; i++){
        if(arr->items[i].id == id){
            return i;
        }
    }
    return -1;
}

/**
 * Hled�n� prvku v poli podle n�zvu. Vrac� index prvku v poli
 * nebo -1, pokud prvek pole neobsahuje.
 */
int array_find_name(Array *arr, char *name)
{
    for(int i = 0; i < arr->size; i++){
        if(arr->items[i].name != NULL && strcmp(arr->items[i].name, name) == 0){
            return i;
        }
    }
    return -1;
}

/**
 * Vlo�en� prvku do pole na zadan� index. Vrac� index vlo�en�ho prvku (idx)
 * nebo -1, pokud se operace nezda�ila.
 */
int array_insert_item(Array *arr, Object *item, unsigned idx)
{
    /** TODO: */
}

/**
 * Hled�n� prvku s nejmen��m identifik�torem. Vrac� index prvku nebo -1,
 * pokud je pole pr�zdn�,
 */
int array_find_min(Array *arr)
{
    /** TODO: */
}

/**
 * �azen� prvk� v poli podle jejich identifik�tor�.
 */
void array_sort(Array *arr)
{
    /** TODO: */
}

/**
 * Vypise prvky pole.
 */
void print_array(Array a)
{
    for (unsigned int i = 0; i < a.size; i++)
    {
        printf("#%d\t", i);
        print_object(a.items[i]);
    }
    printf("\n");
}

/**
 * Vypise prvky pole dane velikosti.
 */
void print_array_size(Array a, unsigned int size)
{
    unsigned int i;
    for (i = 0; i < size; i++)
    {
        printf("#%d\t", i);
        print_object(a.items[i]);
    }
    printf("\n");
}


