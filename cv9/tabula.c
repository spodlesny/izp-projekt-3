typedef struct {
  int id;
  char *name;
} Object;
 
/**
 * Inizializace objektu. Název objektu kopíruje. Objekt bude mít název
 * roven NULL, pokud se inicializace nezdařila. 
 */
Object object_ctor(int id, char *name){
	char *pom = NULL;
	*name = malloc(sizeof(char)*strlen(*name));
	if(name!=NULL){
		*pom = malloc(sizeof(char)*strlen(name));
		if(pom != NULL){
			strcpy(pom, name);
		}
	}

	Object n_obj = {id=id, name=pom};
	return n_obj;

}

 
/**
 * Záměna dat dvou objektů.
 */
void object_swap(Object *i1, Object *i2);
 
/**
 * Hluboká kopie objektu src na dst. Vrací dst, pokud se operace povedla,
 * jinak NULL.
 */
Object *object_cpy(Object *dst, Object *src);
 
/**
 * Uvolní objekt (resp. jeho jméno) z paměti.
 */
void object_dtor(Object *o);