/**
Projekt:    Kostra 9. cviceni IZP 2015
Autor:      Marek Zak <izakmarek@fit.vutbr.cz>
Datum:      28. 11. 2015
*/

#include "list.h"

/**
 * Inicializace seznamu. Vytvo�� pr�zdn� seznam.
 */
List list_ctor()
{
    /** TODO: */
}

/**
 * Inicializace polo�ky seznamu. Z objektu vytvo�� polo�ku bez n�sledn�ka.
 */
Item item_ctor(Object data)
{
    /** TODO: */
}

/**
 * Vlo�� polo�ku na za��tek seznamu.
 */
void list_insert_first(List *list, Item *i)
{
    /** TODO: */
}

/**
 * Vr�t� po�et polo�ek seznamu.
 */
unsigned list_count(List *list)
{
    /** TODO: */
}

/**
 * Vr�t� true, pokud je seznam pr�zdn�.
 */
bool list_empty(List *list)
{
    /** TODO: */
}

/**
 * Najde polo�ku seznamu s nejmen��m identifik�torem. Vrac� NULL, pokud je
 * seznam pr�zdn�.
 */
Item *list_find_minid(List *list)
{
    /** TODO: */
}

/**
 * Najde polo�ku seznamu s odpov�daj�c�m jm�nem objektu. Vrac� NULL, pokud
 * takov� polo�ka v seznamu nen�.
 */
Item *list_find_name(List *list, char *name)
{
    /** TODO: */
}

/**
 * Vytiske polozky seznamu.
 */
void print_list(List l)
{
    if (l.first != NULL)
    {
        printf("#0\t");
        print_object(l.first->data);

        Item *next = l.first->next;
        int i = 1;
        while (next != NULL)
        {
            printf("#%d\t", i);
            print_object(next->data);
            next = next->next;
            i++;
        }
    }
}

/**
 * Vytiske polozky seznamu.
 */
void print_plist(List *l)
{
    if (l->first != NULL)
    {
        printf("#0\t");
        print_object(l->first->data);

        Item *next = l->first->next;
        int i = 1;
        while (next != NULL)
        {
            printf("#%d\t", i);
            print_object(next->data);
            next = next->next;
            i++;
        }
    }
}
