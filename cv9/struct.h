/**
Projekt:    Kostra 9. cviceni IZP 2015
Autor:      Marek Zak <izakmarek@fit.vutbr.cz>
Datum:      28. 11. 2015
*/

#ifndef STRUCT_H_INCLUDED
#define STRUCT_H_INCLUDED

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/**
 * struktura Object
 */
typedef struct {
  int id;
  char *name;
} Object;

/**
 * Inizializace objektu. N�zev objektu kop�ruje. Objekt bude m�t n�zev
 * roven NULL, pokud se inicializace nezda�ila.
 */
Object object_ctor(int id, char *name);

/**
 * Z�m�na dat dvou objekt�.
 */
void object_swap(Object *i1, Object *i2);

/**
 * Hlubok� kopie objektu src na dst. Vrac� dst, pokud se operace povedla,
 * jinak NULL.
 */
Object *item_cpy(Object *dst, Object *src);

/**
 * Vytiske hodnoty daneho objektu.
 */
void print_object(Object o);

void print_pobject(Object *o);

#endif // STRUCT_H_INCLUDED
