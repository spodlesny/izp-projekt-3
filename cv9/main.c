/**
Projekt:    Kostra 9. cviceni IZP 2015
Autor:      Marek Zak <izakmarek@fit.vutbr.cz>
Datum:      28. 11. 2015
*/

#include <stdio.h>
#include "struct.h"
#include "array.h"
#include "list.h"

/**
 * funkce main
 */
int main()
{
    /** ********* testovani funkci struktury ********** */

    printf("========== STRUCT TESTING ==========\n\n");


    /** vytvoreni objektu */
    Object o1 = object_ctor(1, "Hello 1");
    Object o2 = object_ctor(2, "Hello 2");

    printf("INIT OBJECTS\n");
    print_object(o1);
    print_object(o2);


    /** zamena objektu */
    printf("\nSWAP\n");
    object_swap(&o1, &o2);

    print_object(o1);
    print_object(o2);


    /** kopirovani objektu */
    printf("\nCOPY\n");
    Object copy;
    print_pobject(item_cpy(&copy, &o1));


    /** smazani hodnot zdrojoveho objektu */
//    o1.id = -1;
//    o1.name = "---";
//
//    print_object(copy);




    /** ********* testovani funkci pole ********** */

    printf("\n\n========== ARRAY TESTING ==========\n\n");


    /** vytvori pole velikosti ARRAY_SIZE */
    Array a = array_ctor(ARRAY_SIZE);

    printf("INIT ARRAY\n");
    print_array_size(a, ARRAY_SIZE);


    /** zmensi velikost pole */
//    array_resize(&a, SMALLER_ARRAY_SIZE);
//
//    printf("\n\nSMALLER ARRAY\n");
//
//    if (a.size == SMALLER_ARRAY_SIZE)
//    {
//        print_array_size(a, SMALLER_ARRAY_SIZE);
//    }
//    else
//    {
//        fprintf(stderr, "ERROR: Array does not contains %d Object(s).\n", SMALLER_ARRAY_SIZE);
//    }


    /** zvetsi velikost pole */
//    array_resize(&a, BIGGER_ARRAY_SIZE);
//
//    printf("\n\nBIGGER ARRAY\n");
//
//    if (a.size == BIGGER_ARRAY_SIZE)
//    {
//        print_array_size(a, BIGGER_ARRAY_SIZE);
//    }
//    else
//    {
//        fprintf(stderr, "ERROR: Array does not contains %d Object(s).\n", BIGGER_ARRAY_SIZE);
//    }


    /** vyhledani id v poli */
    a.items[SEARCH_ARR_ID_POS].id = SEARCH_ARR_ID;

    printf("\n\nSEARCH ARRAY ID\n");

    printf("ID POSITION: %d, SHOULD BE %d\n", array_find_id(&a, SEARCH_ARR_ID), SEARCH_ARR_ID_POS);


    /** vyhledani name v poli */

    a.items[SEARCH_ARR_NAME_POS].name = SEARCH_ARR_NAME;

    printf("\n\nSEARCH ARRAY NAME\n");

    printf("NAME POSITION: %d, SHOULD BE %d\n", array_find_name(&a, SEARCH_ARR_NAME), SEARCH_ARR_NAME_POS);


    /** vlozeni prvku na zadanou pozici */
//    Object new_item = object_ctor(ADD_ARRAY_ID, ADD_ARRAY_NAME);
//
//    a.items[ADD_ARRAY_POS].id = ACT_ARRAY_ID;
//    a.items[ADD_ARRAY_POS].name = ACT_ARRAY_NAME;
//
//    array_insert_item(&a, &new_item, ADD_ARRAY_POS);
//
//    printf("\n\nADD TO ARRAY ON POSITION\n");
//
//    if (a.items[ADD_ARRAY_POS].id == ADD_ARRAY_ID &&
//        strcmp(a.items[ADD_ARRAY_POS].name, ADD_ARRAY_NAME) == 0 &&
//        a.items[ADD_ARRAY_POS + 1].id == ACT_ARRAY_ID &&
//        strcmp(a.items[ADD_ARRAY_POS + 1].name, ACT_ARRAY_NAME) == 0)
//    {
//        printf("Object {%d, '%s'} added to position %d\n", ADD_ARRAY_ID, ADD_ARRAY_NAME, ADD_ARRAY_POS);
//    }
//    else
//    {
//        fprintf(stderr, "Object added unsuccessfully.\n");
//    }


    /** nalezeni minimalniho prvku */
//    Array b = array_ctor(BIGGER_ARRAY_SIZE);
//
//    for (unsigned int i = 0; i < b.size; i++)
//    {
//        b.items[i].id = (i * 5 + 7) % BIGGER_ARRAY_SIZE;
//    }
//
//    printf("\n\nMIN ITEM POSITION\n");
//
//    print_array(b);
//
//    printf("MINIMUM: %d\n", array_find_min(&b));
//
//
//    /** serazeni pole */
//    printf("\n\nSORT ARRAY ITEMS\n");
//
//    array_sort(&b);
//
//    print_array(b);




    /** ********* testovani funkci seznamu ********** */

    printf("\n\n========== LIST TESTING ==========\n\n");

    /** inicializace seznamu */
//    printf("\n\nINIT LIST\n");
//
//    List l = list_ctor();


    /** inicializace polozky seznamu */
//    printf("\n\nINIT LIST ITEM\n");
//
//    Object ol1 = object_ctor(LIST_ITEM_ID, LIST_ITEM_NAME);
//    Item i1 = item_ctor(ol1);
//
//    Object ol2 = object_ctor(LIST_ITEM_2_ID, LIST_ITEM_2_NAME);
//    Item i2 = item_ctor(ol2);
//
//    if (i1.data.id == LIST_ITEM_ID && strcmp(i1.data.name, LIST_ITEM_NAME) == 0 && i1.next == NULL &&
//        i2.data.id == LIST_ITEM_2_ID && strcmp(i2.data.name, LIST_ITEM_2_NAME) == 0 && i2.next == NULL)
//    {
//        printf("List item init OK.\n");
//    }
//    else
//    {
//        fprintf(stderr, "List item init failed.\n");
//    }


    /** vlozeni polozky na zacatek seznamu */
//    printf("\n\nINSERT LIST ITEM\n");
//
//    list_insert_first(&l, &i1);
//
//    if (l.first == &i1 && l.first->next == NULL)
//    {
//        list_insert_first(&l, &i2);
//
//        if (l.first == &i2 && l.first->next == &i1 && l.first->next->next == NULL)
//        {
//            printf("List item init OK.\n");
//        }
//        else
//        {
//          fprintf(stderr, "List item init failed.\n");
//        }
//    }
//    else
//    {
//        fprintf(stderr, "List item init failed.\n");
//    }
//
//    Object ol3 = object_ctor(LIST_ITEM_2_ID + 1, LIST_ITEM_NAME);
//    Item i3 = item_ctor(ol3);
//    list_insert_first(&l, &i3);
//
//    Object ol4 = object_ctor(LIST_ITEM_2_ID + 2, LIST_ITEM_2_NAME);
//    Item i4 = item_ctor(ol4);
//    list_insert_first(&l, &i4);
//
//    print_list(l);


    /** pocet polozek v seznamu */
//    printf("\n\nLIST ITEMS COUNT\n");
//
//    printf("List item count: %d, should be 4.\n", list_count(&l));
//
//
//    /** prazdny seznam */
//    printf("\n\nEMPTY LIST\n");
//
//    List l2 = list_ctor();
//
//    if (!list_empty(&l) && list_empty(&l2))
//    {
//        printf("Empty list OK.\n");
//    }
//    else
//    {
//        fprintf(stderr, "Empty list test failed.\n");
//    }


    /** nalezeni nejmensiho ID v seznamu */
//    printf("\n\nFIND MIN ID\n");
//
//    l.first->next->data.id = 0;
//    l.first->next->data.name = "Min";
//    Item *imin = NULL;
//
//    if ((imin = list_find_minid(&l)) != NULL && imin->data.id == 0)
//    {
//        printf("Find min ID OK.\n");
//    }
//    else
//    {
//        fprintf(stderr, "Min ID not found.\n");
//    }


    /** nalezeni polozky s danym jmenem v seznamu */
//    printf("\n\nFIND MIN ID\n");
//
//    l.first->next->next->data.id = LIST_FIND_NAME_ID;
//    l.first->next->next->data.name = LIST_FIND_NAME;
//    Item *iname = NULL;
//
//    if ((iname = list_find_name(&l, LIST_FIND_NAME)) != NULL && iname->data.id == LIST_FIND_NAME_ID)
//    {
//        printf("Find item with name %s OK.\n", LIST_FIND_NAME);
//    }
//    else
//    {
//        fprintf(stderr, "Item with name %s not found.\n", LIST_FIND_NAME);
//    }

    return 0;
}
