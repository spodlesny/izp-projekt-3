/**
Projekt:    Kostra 9. cviceni IZP 2015
Autor:      Marek Zak <izakmarek@fit.vutbr.cz>
Datum:      28. 11. 2015
*/

#include "struct.h"

/**
 * Inizializace objektu. N�zev objektu kop�ruje. Objekt bude m�t n�zev
 * roven NULL, pokud se inicializace nezda�ila.
 */
Object object_ctor(int id, char *name)
{
    //*name = malloc(sizeof(char)*strlen(*name));
    char *pom = NULL;
	if(name != NULL){
		pom = malloc(sizeof(char)*strlen(name));
		}
    if(pom != NULL){
        strcpy(pom, name);
    }


	Object n_obj = {id=id, name=pom};
	return n_obj;
}

/**
 * Z�m�na dat dvou objekt�.
 */
void object_swap(Object *i1, Object *i2)
{
    Object tmp;
    tmp.id = i1->id;
    tmp.name = i1->name;

    i1->id = i2 -> id;
    i1->name = i2->name;

    i2->id = tmp.id;
    i2->name=tmp.name;
}

/**
 * Hlubok� kopie objektu src na dst. Vrac� dst, pokud se operace povedla,
 * jinak NULL.
 */
Object *item_cpy(Object *dst, Object *src)
{
    *dst = object_ctor(src->id, src -> name);
    if(dst->name == NULL){
        return NULL;
    }
    else{
        return dst;
    }
}

/**
 * Vytiske hodnoty daneho objektu.
 */
void print_object(Object o)
{
    printf("ID: %d, NAME: %s\n", o.id, o.name);
}

/**
 * Vytiske hodnoty daneho objektu.
 */
void print_pobject(Object *o)
{
    printf("ID: %d, NAME: %s\n", o->id, o->name);
}
