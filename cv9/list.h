/**
Projekt:    Kostra 9. cviceni IZP 2015
Autor:      Marek Zak <izakmarek@fit.vutbr.cz>
Datum:      28. 11. 2015
*/

#ifndef LIST_H_INCLUDED
#define LIST_H_INCLUDED

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "struct.h"

#define LIST_ITEM_ID        1
#define LIST_ITEM_NAME      "List Item 1"
#define LIST_ITEM_2_ID      2
#define LIST_ITEM_2_NAME    "List Item 2"
#define LIST_FIND_NAME_ID   100
#define LIST_FIND_NAME      "IZP"

typedef struct item {
  Object data;
  struct item *next;
} Item;

typedef struct {
  Item *first;
} List;

/**
 * Inicializace seznamu. Vytvo�� pr�zdn� seznam.
 */
List list_ctor();

/**
 * Inicializace polo�ky seznamu. Z objektu vytvo�� polo�ku bez n�sledn�ka.
 */
Item item_ctor(Object data);

/**
 * Vlo�� polo�ku na za��tek seznamu.
 */
void list_insert_first(List *list, Item *i);

/**
 * Vr�t� po�et polo�ek seznamu.
 */
unsigned list_count(List *list);

/**
 * Vr�t� true, pokud je seznam pr�zdn�.
 */
bool list_empty(List *list);

/**
 * Najde polo�ku seznamu s nejmen��m identifik�torem. Vrac� NULL, pokud je
 * seznam pr�zdn�.
 */
Item *list_find_minid(List *list);

/**
 * Najde polo�ku seznamu s odpov�daj�c�m jm�nem objektu. Vrac� NULL, pokud
 * takov� polo�ka v seznamu nen�.
 */
Item *list_find_name(List *list, char *name);

/**
 * Vytiske polozky seznamu.
 */
void print_list(List l);

/**
 * Vytiske polozky seznamu.
 */
void print_plist(List *l);

#endif // LIST_H_INCLUDED
