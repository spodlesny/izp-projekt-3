/**
 * Kostra hlavickoveho souboru 3. projekt IZP 2015/16
 * a pro dokumentaci Javadoc.
 *
 * @author Simon Podlesny (xpodle01)
 *
 * @defgroup auxiliary Auxiliary functions
 * @defgroup array_of_clusters Work with array of clusters
 * @defgroup clusters Work with clusters
 * @defgroup objects Work with objects
 *
 * @mainpage Cluster analysis
 * @image html http://i.imgur.com/pd42Uds.gif
 */

/**
 * @brief      Structure of cluster object
 */
struct obj_t {
	/// ID of object
    int id;
    /// X coordinate
    float x;
    /// Y coordinate
    float y;
};

/**
 * @brief      Structure of cluster.
 * 
 */
struct cluster_t {
	/// Size of cluster
    int size;
    /// Capacity of cluster
    int capacity;
    /// Pointer on structure obj_t
    struct obj_t *obj;
};

/**
 * @ingroup  clusters
 * @brief      Initialize cluster 'c' and alocate memory for 'cap' (capacity) of object.
 * 
 * @param      c     Cluster that should be initialize
 * @param[in]  cap   Size allocated memory
 * 
 * @pre      Cluster c cannot be NULL and capacity need to be bigger then 0
 * @post     NULL pointer means capacity 0
 */
void init_cluster(struct cluster_t *c, int cap);

/**
 * @ingroup  clusters
 * @brief      Remove all objects from cluster and initialize empty cluster
 * 
 * @param      c     Pointer on cluster that should be cleared
 */
void clear_cluster(struct cluster_t *c);

/**
 * @ingroup  clusters
 * Chunk of cluster objects. Value recommended for reallocation.
 */
extern const int CLUSTER_CHUNK;

/**
 * @ingroup  clusters
 * @brief      Change cluster capacity
 * 
 *
 * @param      c        Cluster that should be resized
 * @param[in]  new_cap  New capacity of cluster
 *
 * @return     Return resized cluster
 * @pre cluster Capacity and new_capacity need to be bigger then 0
 */
struct cluster_t *resize_cluster(struct cluster_t *c, int new_cap);

/**
 * @ingroup  clusters
 * @brief      Add new object at end of cluster. Resize cluster if is it needed
 *
 * @param      c     Cluster that will be appended
 * @param[in]  obj   Object that should be appended
 */
void append_cluster(struct cluster_t *c, struct obj_t obj);

/**
 * @ingroup  clusters
 * @brief      Add objects from cluster 'c2' to cluster 'c1' and resize cluster if is it needed. 
 * Objects in 'c1' will be sorted. Cluster c2 will not be changed 
 * 
 *
 * @param      c1    Cluster that will be appended
 * @param      c2    Cluster from which will be objects copied }
 * 
 * @pre      Struct c1 and c2 cannot be NULL
 * @post     Struct c2 cannot be changed
 */
void merge_clusters(struct cluster_t *c1, struct cluster_t *c2);

/**
 * @ingroup  array_of_clusters
 * @brief      Remove cluster from array
 * 
 * @param      carr  Array of clusters
 * @param[in]  narr  Number of objects that cluster contains
 * @param[in]  idx   Position (index) of cluster that should be removed
 *
 * @return     New array size
 * @pre      Variable idx need to be lower then narr 
 * @pre      Variable narr need to be bigger then 0
 */
int remove_cluster(struct cluster_t *carr, int narr, int idx);

/**
 * @ingroup  objects
 * @brief      Calculate Euklidean distance between two objects
 *
 * @param      o1    Object 1
 * @param      o2    Object 2
 *
 * @return     Euklidean distance
 * @pre      Objects o1 and o2 cannot be NULL
 */
float obj_distance(struct obj_t *o1, struct obj_t *o2);

/**
 * @ingroup  clusters
 * @brief      Calculate distance between two clusters based on Euklidean distance
 *
 * @param      c1    Cluster 1
 * @param      c2    Cluster 2
 *
 * @return     Distance between two clusters
 * 
 * @pre Objects o1 and o2 cannot be empty
 * @pre Size of objects o1 and o2 need to be bigger then 0
 */
float cluster_distance(struct cluster_t *c1, struct cluster_t *c2);

/**
 * @ingroup  array_of_clusters
 * @brief      Function will find two nearest clusters and save this indexes into c1, c2
 *
 * @param      carr  Array of clusters
 * @param[in]  narr  Size of carr
 * @param      c1    Index position of first nearest cluster
 * @param      c2    Index position of second nearest cluster
 * 
 * @pre Variable narr need to be bigger then 0
 */
void find_neighbours(struct cluster_t *carr, int narr, int *c1, int *c2);

/**
 * @ingroup  clusters
 * @brief      Sort objects ascending by they indentificator
 *
 * @param      c     Cluster that will be sorted
 */
void sort_cluster(struct cluster_t *c);

/**
 * @ingroup  auxiliary
 * @brief      Print cluster on stdout
 *
 * @param      c     Cluster that will be printed
 */
void print_cluster(struct cluster_t *c);

/**
 * @ingroup  array_of_clusters
 * @brief      Load objects from file and create new cluster for every object. Then append it to array of clusters and allocate memory for array. 
 *
 * @param      filename  Location of file with objects
 * @param      arr       Pointer on first object in array (cluster). If error happend arr contains NULL as value
 *
 * @return     Number of loaded clusters
 * @pre      Variable arr cannot be NULL
 * @post     If memory cannot be allocated, arr is NULL and function return -1
 * @post     If file cannot be open, return of funtion is -2 
 * @post     If function cannot load proper value size, function return -3
 */
int load_clusters(char *filename, struct cluster_t **arr);

/**
 * @ingroup  auxiliary
 * @brief      Print array of clusters
 *
 * @param      carr  Pointer on first cluster in array
 * @param[in]  narr  Number of clusters that sgould be printed
 */
void print_clusters(struct cluster_t *carr, int narr);
