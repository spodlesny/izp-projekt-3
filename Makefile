CFLAGS=-std=c99 -Wall -Wextra -pedantic -Wno-unused-parameter -Wno-return-type
LDFLAGS=-lm

all: proj3.c
	$(CC) $(CFLAGS) proj3.c -o proj3 $(LDFLAGS)

clean:
	rm ./proj3

.PHONY: clean